$ ->
  # smooth scroll
  # $('a[href*=#]').on 'click', (event) ->
  #   event.preventDefault()
  #   $('html, body').animate { scrollTop: $(@hash).offset().top }, 500

  $('#nav-lang').on 'click', (e) ->
    $('#nav-lang-alt').fadeIn()

  # home slide
  $('#home-products').slick
    arrows: false
    adaptiveHeight: true

  $('.gallery').slick
    adaptiveHeight: true
    prevArrow: '<a class="slick-prev hidden-xs"></a>'
    nextArrow: '<a class="slick-next hidden-xs"></a>'

  # popovers
  $('[data-toggle="popover"]').popover()

  $('#home-products .circle').on 'click', (e) ->
    slide = $(this).attr 'data-slide'
    $('#home-products').slick 'slickGoTo', slide

  $('#nav-expander').on 'click', (e) ->
    e.preventDefault()
    $('#nav-expander').toggleClass 'active'
    $('body').toggleClass 'nav-expanded'

  $('.flag-collapse').on 'click', (e) ->
    e.preventDefault()
    $('.flag-collapse').not($(this)).toggle()
    $(this).find('span').toggleClass('icon-plus-circle icon-circle-minus')


  url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.stocks%20where%20symbol%3D%22luve.it%22&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback='

  if $('#stocks-ticker').length

    $.getJSON 'http://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', (data) ->

      # Create the chart
      $('#stocks-ticker').highcharts 'StockChart',
        chart:
          backgroundColor: '#082c51'
        labels:
          style:
            color: '#ffffff'
        rangeSelector:
          enabled: false
        scrollbar:
          enabled: false
        navigator:
          enabled: false
        tooltip:
          enabled: false
        xAxis:
          labels:
            style:
              color: 'white'
        yAxis:
          opposite: false
          labels:
            style:
              color: 'white'
        series: [ {
          data: data
          type: 'area'
          threshold: null
          tooltip: valueDecimals: 2
          fillColor: '#7fb0d8'
        } ]


$(document).ready ->

  dotnavigation = ->
    numSections = $('section').length
    $('#dot-nav li a').removeClass('active').parent('li').removeClass 'active'
    $('#dot-nav-mob li a').removeClass('active').parent('li').removeClass 'active'
    $('section').each (i, item) ->
      ele = $(item)
      nextTop = undefined
      if typeof ele.next().offset() != 'undefined'
        nextTop = ele.next().offset().top
      else
        nextTop = $(document).height()

      if ele.offset() != null
        thisTop = ele.offset().top - ((nextTop - (ele.offset().top)) / numSections)
      else
        thisTop = 0

      docTop = $(document).scrollTop()

      if docTop >= thisTop and docTop < nextTop
        $('#dot-nav li a').removeClass('active').parent('li').removeClass 'active'
        $('#dot-nav-mob li a').removeClass('active').parent('li').removeClass 'active'
        $('#dot-nav li').eq(i).addClass 'active'
        $('#dot-nav-mob li').eq(i).addClass 'active'

  $('.awesome-tooltip').tooltip placement: 'left'
  $(window).bind 'scroll', (e) ->
    dotnavigation()

$(document).scroll ->
  y = $(this).scrollTop()
  if y > 150
    $('#dot-nav').slideDown()
    $('#dot-nav-mob').css('opacity', 1)
  else
    $('#dot-nav').slideUp()
    $('#dot-nav-mob').css('opacity', 0)
