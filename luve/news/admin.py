from django.contrib import admin
from suit.admin import SortableModelAdmin
from modeltranslation.admin import TranslationAdmin

from .models import News


class NewsAdmin(TranslationAdmin, SortableModelAdmin):
    list_display = ('title', 'date')
    sortable = 'order'


admin.site.register(News, NewsAdmin)
