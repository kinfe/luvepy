# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(max_length=200)),
                ('date', models.DateField()),
                ('slug', models.SlugField(max_length=100)),
                ('content', models.TextField(blank=True)),
                ('order', models.PositiveIntegerField()),
            ],
        ),
    ]
