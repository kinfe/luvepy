# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20150823_2346'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='abstract_en',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='news',
            name='abstract_it',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='news',
            name='content_en',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='news',
            name='content_it',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='news',
            name='slug_en',
            field=models.SlugField(null=True, max_length=100),
        ),
        migrations.AddField(
            model_name='news',
            name='slug_it',
            field=models.SlugField(null=True, max_length=100),
        ),
        migrations.AddField(
            model_name='news',
            name='title_en',
            field=models.CharField(null=True, max_length=200),
        ),
        migrations.AddField(
            model_name='news',
            name='title_it',
            field=models.CharField(null=True, max_length=200),
        ),
    ]
