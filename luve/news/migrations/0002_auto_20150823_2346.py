# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='news',
            options={'verbose_name_plural': 'news', 'verbose_name': 'news'},
        ),
        migrations.AddField(
            model_name='news',
            name='abstract',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
