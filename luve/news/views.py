from django.shortcuts import render, get_object_or_404

from .models import News


def index(request):
    news = News.objects.order_by('-date')[:6]

    return render(request, 'news/index.html', {'news': news})


def detail(request, id):
    news = get_object_or_404(News, id=id)

    return render(request, 'news/detail.html', {'news': news})
