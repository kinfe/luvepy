from django.core.urlresolvers import reverse
from django.db import models


class News(models.Model):
    title = models.CharField(max_length=200)
    date = models.DateField()
    abstract = models.CharField(max_length=255)
    slug = models.SlugField(max_length=100)
    content = models.TextField(blank=True)
    order = models.PositiveIntegerField()

    def get_absolute_url(self):
        return reverse('luve.news.views.detail', args=[self.id])

    class Meta:
        verbose_name = 'news'
        verbose_name_plural = 'news'

    def __str__(self):
        return self.title
