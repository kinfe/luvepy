from modeltranslation.translator import register, TranslationOptions
from .models import Page, Attachment


@register(Page)
class PageTranslationOptions(TranslationOptions):
    fields = ('title', 'slug', 'full_slug', 'content')


@register(Attachment)
class AttachmentTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'disclaimer', 'file')
