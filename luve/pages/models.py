from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models, transaction
from django.utils import translation

from mptt.models import MPTTModel, TreeForeignKey

from ..core.formatting import parse_shortcodes


class Page(MPTTModel):
    TEMPLATES = (
        ('static_page', 'menu a sinistra'),
        ('static_page_full', 'senza menu'),
        ('static_page_no_layout', 'senza menu, layout libero'),
    )
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=100)
    full_slug = models.CharField(max_length=255, blank=True, db_index=True)
    content = models.TextField(blank=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    template = models.CharField(max_length=50, choices=TEMPLATES, default="static_page")
    order = models.PositiveIntegerField()

    @property
    def parsed_content(self):
        return parse_shortcodes(self.content)

    @staticmethod
    @transaction.atomic
    def rebuild_order():
        try:
            for k, p in enumerate(Page.objects.order_by('tree_id', 'lft'), start=1):
                print(k)
                p.order = k
                p.save()
        except AttributeError:
            pass

    def attachments(self):
        return Attachment.objects.filter(page=self)

    def get_translated_url(self, lang):
        request_lang = translation.get_language()
        translation.activate(lang)
        return reverse('luve.pages.views.static_page', args=[self.full_slug])
        translation.activate(request_lang)

    def get_absolute_url(self):
        return reverse('luve.pages.views.static_page', args=[self.full_slug])

    # It is required to rebuild tree after save, when using order for mptt-tree
    def save(self, *args, **kwargs):
        super(Page, self).save(*args, **kwargs)

        base_lang = translation.get_language()

        for key, lang in settings.LANGUAGES:
            translation.activate(key)

            full_slug = ''

            if self.get_ancestors().count() > 0:
                for ancestor in self.get_ancestors(include_self=True):
                    slugs = [a.slug for a in self.get_ancestors(include_self=True)]
                    full_slug = '/'.join(slugs)
            else:
                full_slug = self.slug

            self.full_slug = full_slug

        translation.activate(base_lang)

        Page.objects.rebuild()

        super(Page, self).save(*args, **kwargs)

    class MPTTMeta:
        order_insertion_by = ['order']

    class Meta:
        verbose_name = 'pagina'
        verbose_name_plural = 'pagine'

    def __str__(self):
        return self.title


class Attachment(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    disclaimer = models.TextField(blank=True)
    file = models.FileField(upload_to='attachments')
    page = models.ForeignKey('Page')

    def get_absolute_url(self):
        return reverse('luve.pages.views.attachment', args=[self.id])

    class Meta:
        verbose_name = 'allegato'
        verbose_name_plural = 'allegati'

    def __str__(self):
        return '%s - %s' % (self.name, self.file)
