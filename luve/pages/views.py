from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404, redirect, render

from django.contrib.auth.decorators import login_required

from ..news.models import News
from .models import Attachment, Page


def home(request):
    news = News.objects.order_by('-date')[:2]

    return render(request, 'home.html', {'news': news})


def static_page(request, full_slug):
    page = get_object_or_404(Page, full_slug=full_slug)

    if not page.content:
        try:
            return redirect(page.get_children()[0])
        except IndexError:
            pass

    return render(request, '%s.html' % page.template, {'page': page})


@login_required
def rebuild_pages_order(request):
    Page.rebuild_order()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def attachment(request, id):
    attachment = get_object_or_404(Attachment, id=id)

    if attachment.disclaimer:
        return render(request, 'attachment.html', {'attachment': attachment})
    else:
        return redirect(attachment.file.url)
