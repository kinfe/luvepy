# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0005_page_full_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='full_slug',
            field=models.CharField(max_length=255, db_index=True, blank=True),
        ),
    ]
