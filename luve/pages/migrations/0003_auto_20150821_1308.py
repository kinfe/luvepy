# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0002_auto_20150821_1259'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='page',
            name='attachments',
        ),
        migrations.AddField(
            model_name='attachment',
            name='page',
            field=models.ForeignKey(default=1, to='pages.Page'),
            preserve_default=False,
        ),
    ]
