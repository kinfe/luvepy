# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0007_auto_20150823_2346'),
    ]

    operations = [
        migrations.AddField(
            model_name='attachment',
            name='description_en',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='attachment',
            name='description_it',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='attachment',
            name='disclaimer_en',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='attachment',
            name='disclaimer_it',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='attachment',
            name='name_en',
            field=models.CharField(null=True, max_length=200),
        ),
        migrations.AddField(
            model_name='attachment',
            name='name_it',
            field=models.CharField(null=True, max_length=200),
        ),
        migrations.AddField(
            model_name='page',
            name='content_en',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='page',
            name='content_it',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='page',
            name='full_slug_en',
            field=models.CharField(null=True, max_length=255, blank=True, db_index=True),
        ),
        migrations.AddField(
            model_name='page',
            name='full_slug_it',
            field=models.CharField(null=True, max_length=255, blank=True, db_index=True),
        ),
        migrations.AddField(
            model_name='page',
            name='slug_en',
            field=models.SlugField(null=True, max_length=100),
        ),
        migrations.AddField(
            model_name='page',
            name='slug_it',
            field=models.SlugField(null=True, max_length=100),
        ),
        migrations.AddField(
            model_name='page',
            name='title_en',
            field=models.CharField(null=True, max_length=200),
        ),
        migrations.AddField(
            model_name='page',
            name='title_it',
            field=models.CharField(null=True, max_length=200),
        ),
    ]
