# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0006_auto_20150823_1759'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attachment',
            options={'verbose_name_plural': 'allegati', 'verbose_name': 'allegato'},
        ),
        migrations.AlterModelOptions(
            name='page',
            options={'verbose_name_plural': 'pagine', 'verbose_name': 'pagina'},
        ),
    ]
