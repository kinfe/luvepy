# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True)),
                ('disclaimer', models.TextField(blank=True)),
                ('file', models.FileField(upload_to='attachments')),
            ],
        ),
        migrations.AddField(
            model_name='page',
            name='attachments',
            field=models.ManyToManyField(to='pages.Attachment'),
        ),
    ]
