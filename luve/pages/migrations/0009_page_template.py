# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0008_auto_20150824_1544'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='template',
            field=models.CharField(choices=[('static_page', 'menu a sinistra'), ('static_page_full', 'senza menu')], max_length=50, default='static_page'),
        ),
    ]
