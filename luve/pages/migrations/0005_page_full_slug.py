# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0004_page_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='full_slug',
            field=models.CharField(default='slug', max_length=300),
            preserve_default=False,
        ),
    ]
