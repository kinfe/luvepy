# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0010_auto_20151001_1724'),
    ]

    operations = [
        migrations.AddField(
            model_name='attachment',
            name='file_en',
            field=models.FileField(upload_to='attachments', null=True),
        ),
        migrations.AddField(
            model_name='attachment',
            name='file_it',
            field=models.FileField(upload_to='attachments', null=True),
        ),
    ]
