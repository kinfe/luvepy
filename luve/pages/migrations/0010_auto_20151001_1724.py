# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0009_page_template'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='template',
            field=models.CharField(max_length=50, default='static_page', choices=[('static_page', 'menu a sinistra'), ('static_page_full', 'senza menu'), ('static_page_no_layout', 'senza menu, layout libero')]),
        ),
    ]
