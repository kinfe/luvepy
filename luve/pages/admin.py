from django.contrib import admin

from modeltranslation.admin import TranslationAdmin, TranslationStackedInline
from mptt.admin import MPTTModelAdmin
from suit.admin import SortableModelAdmin

from .models import Page, Attachment


class AttachmentInline (TranslationStackedInline):
    model = Attachment
    extra = 0


class PageAdmin(MPTTModelAdmin, TranslationAdmin, SortableModelAdmin):
    inlines = [AttachmentInline]
    change_list_template = 'admin/pages/mptt_change_list.html'
    mptt_level_indent = 20
    list_display = ('title', 'full_slug')
    exclude = ('full_slug',)
    sortable = 'order'


admin.site.register(Page, PageAdmin)
