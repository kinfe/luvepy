from django.contrib import admin
from django.contrib.contenttypes.generic import GenericTabularInline

from modeltranslation.admin import TranslationAdmin, TranslationTabularInline
from solo.admin import SingletonModelAdmin
from suit.admin import SortableTabularInline, SortableModelAdmin

from .models import SiteConfiguration, Gallery, Picture, Menu


class PictureInline(SortableTabularInline, TranslationTabularInline, GenericTabularInline):
    model = Picture
    fields = ('name', 'file')
    extra = 2


class GalleryAdmin(admin.ModelAdmin):
    inlines = [PictureInline]
    list_display = ('title', 'id')


class MenuAdmin(TranslationAdmin, SortableModelAdmin):
    list_display = ('title',)
    sortable = 'order'


class SiteConfigurationAdmin(SingletonModelAdmin, TranslationAdmin):
    pass


admin.site.register(Gallery, GalleryAdmin)
admin.site.register(Menu, MenuAdmin)
admin.site.register(SiteConfiguration, SiteConfigurationAdmin)
