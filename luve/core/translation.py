from modeltranslation.translator import register, TranslationOptions
from .models import Picture, Menu, SiteConfiguration


@register(Picture)
class PictureTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Menu)
class MenuTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(SiteConfiguration)
class SiteConfigurationTranslationOptions(TranslationOptions):
    fields = ('home_text',)
