# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150909_2311'),
    ]

    operations = [
        migrations.AddField(
            model_name='picture',
            name='name_en',
            field=models.CharField(null=True, max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='picture',
            name='name_it',
            field=models.CharField(null=True, max_length=255, blank=True),
        ),
    ]
