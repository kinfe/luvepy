# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0011_auto_20151008_1533'),
        ('core', '0007_auto_20151107_1935'),
    ]

    operations = [
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('title', models.CharField(max_length=255)),
                ('title_it', models.CharField(max_length=255, null=True)),
                ('title_en', models.CharField(max_length=255, null=True)),
                ('dynamic_route', models.CharField(blank=True, max_length=50)),
                ('order', models.PositiveIntegerField(blank=True, null=True)),
                ('static_page_link', models.ForeignKey(to='pages.Page')),
            ],
            options={
                'verbose_name': 'voce di menu',
                'verbose_name_plural': 'voci di menu',
            },
        ),
    ]
