# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20150910_0058'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='home_text_en',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='siteconfiguration',
            name='home_text_it',
            field=models.TextField(null=True, blank=True),
        ),
    ]
