# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_menu'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menu',
            name='static_page_link',
            field=models.ForeignKey(null=True, blank=True, to='pages.Page'),
        ),
    ]
