# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20151001_1724'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='map_commerciali',
            field=models.ImageField(default=0, upload_to='pictures', help_text="L'immagine caricata dev'essere uguale, nelle proporzioni, a quella precedente."),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='siteconfiguration',
            name='map_produttive',
            field=models.ImageField(default=0, upload_to='pictures', help_text="L'immagine caricata dev'essere uguale, nelle proporzioni, a quella precedente."),
            preserve_default=False,
        ),
    ]
