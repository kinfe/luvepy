# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20150909_2322'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='gallery',
            options={'verbose_name_plural': 'gallery', 'verbose_name': 'gallery'},
        ),
        migrations.AlterModelOptions(
            name='picture',
            options={'verbose_name_plural': 'immagini', 'verbose_name': 'immagine'},
        ),
        migrations.AddField(
            model_name='siteconfiguration',
            name='home_text',
            field=models.TextField(blank=True),
        ),
    ]
