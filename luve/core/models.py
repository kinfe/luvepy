import datetime

from django.db import models

from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType

from solo.models import SingletonModel


class SiteConfiguration(SingletonModel):
    footer_contacts = models.TextField(blank=True)
    home_text = models.TextField(blank=True)
    map_commerciali = models.ImageField(upload_to="pictures", help_text="L'immagine caricata dev'essere uguale, nelle proporzioni, a quella precedente.")
    map_produttive = models.ImageField(upload_to="pictures", help_text="L'immagine caricata dev'essere uguale, nelle proporzioni, a quella precedente.")

    def __str__(self):
        return u"Configurazione"

    class Meta:
        verbose_name = "Configurazione"


class Picture(models.Model):
    file = models.ImageField(upload_to="pictures")
    name = models.CharField(max_length=255, blank=True, null=True)
    order = models.PositiveIntegerField(blank=True, null=True)
    date_created = models.DateTimeField(default=datetime.datetime.now)
    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey()

    def __str__(self):
        return '%s (%s)' % (self.name, self.file.name)

    class Meta:
        verbose_name = 'immagine'
        verbose_name_plural = 'immagini'


class Gallery(models.Model):
    title = models.CharField(max_length=255)
    pictures = GenericRelation(Picture)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'gallery'
        verbose_name_plural = 'gallery'


class Menu(models.Model):
    title = models.CharField(max_length=255)
    static_page_link = models.ForeignKey('pages.Page', blank=True, null=True)
    dynamic_route = models.CharField(max_length=50, blank=True)
    order = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'voce di menu'
        verbose_name_plural = 'voci di menu'
