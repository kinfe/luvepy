import re

from django.template.loader import render_to_string

from ..core.models import Gallery


def parse_shortcodes(text):
    if not text:
        return ''
    else:
        for match in re.finditer(r"\[gallery\=(\d*)\]", text):
            gallery = Gallery.objects.get(id=match.group(1))
            gallery_html = render_to_string('_gallery.html', {'gallery': gallery})
            text = text.replace('[gallery=%s]' % match.group(1), gallery_html, 1)

        return text
