from django import template

from ..models import Menu

register = template.Library()


@register.assignment_tag
def menu_items():
    return Menu.objects.all()
