from django import template
from django.core.urlresolvers import reverse
from django.core.urlresolvers import resolve
from django.utils import translation

from luve.pages.models import Page


register = template.Library()


class TranslatedURL(template.Node):
    def __init__(self, language):
        self.language = language

    def render(self, context):
        view = resolve(context['request'].path)
        request_language = translation.get_language()

        if view.url_name == 'static_page':
            if request_language == 'en':
                page = Page.objects.get(full_slug_en=view.kwargs['full_slug'])
            else:
                page = Page.objects.get(full_slug=view.kwargs['full_slug'])
            translation.activate(self.language)
            url = reverse(view.url_name, args=view.args, kwargs={'full_slug': page.full_slug})
        else:
            translation.activate(self.language)
            url = reverse(view.url_name, args=view.args, kwargs=view.kwargs)

        translation.activate(request_language)
        return url


@register.tag(name='translate_url')
def do_translate_url(parser, token):
    language = token.split_contents()[1]
    return TranslatedURL(language)
