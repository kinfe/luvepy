import json

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from django.contrib.auth.decorators import login_required

from .models import Picture


@csrf_exempt
@require_POST
@login_required
def upload_pictures(request):
    images = []
    for f in request.FILES.getlist("file"):
        obj = Picture.objects.create(file=f)
        images.append({"filelink": obj.file.url})
    return HttpResponse(json.dumps(images), content_type="application/json")


@login_required
def recent_pictures(request):
    images = [
        {"thumb": obj.file.url, "image": obj.file.url} for obj in Picture.objects.order_by("-date_created")[:20]
    ]
    return HttpResponse(json.dumps(images), content_type="application/json")
