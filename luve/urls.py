"""luve URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView

from django.contrib import admin

urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/', include(admin.site.urls)),
    url(r"^pages/rebuild/$", "luve.pages.views.rebuild_pages_order", name="rebuild_pages_order"),
    url(r"^ajax/pictures/upload/$", "luve.core.views.upload_pictures", name="upload_pictures"),
    url(r"^ajax/pictures/recent/$", "luve.core.views.recent_pictures", name="recent_pictures"),
    url(r'^certificazioni/', TemplateView.as_view(template_name='certificazioni.html')),
]

urlpatterns += i18n_patterns(
    url(r'^$', 'luve.pages.views.home', name='home'),
    url(_(r'^societa-produttive/'), 'luve.companies.views.produttive', name='societa_produttive'),
    url(_(r'^societa-commerciali/'), 'luve.companies.views.commerciali', name='societa_commerciali'),
    url(r'^news/$', 'luve.news.views.index', name='news_index'),
    url(r'^news/(?P<id>[-\w]+)$$', 'luve.news.views.detail', name='news_detail'),
    url(_(r'^contatti/'), TemplateView.as_view(template_name='contatti.html'), name='contacts'),
    url(r'^attachments/(?P<id>[-\w]+)$$', 'luve.pages.views.attachment', name='attachment'),
    url(r'^(?P<full_slug>.*)/$', 'luve.pages.views.static_page', name='static_page'),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
