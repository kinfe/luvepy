from django.shortcuts import render

from .models import Company


def produttive(request):
    companies = Company.objects.filter(type=1).order_by('country', 'name')

    return render(request, 'societa_produttive.html', {'companies': companies})


def commerciali(request):
    companies = Company.objects.filter(type=2).order_by('country', 'name')

    return render(request, 'societa_commerciali.html', {'companies': companies, 'hq': True})
