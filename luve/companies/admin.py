from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from .models import Company


class CompanyAdmin(TranslationAdmin):
    list_display = ('name', 'country', 'type')


admin.site.register(Company, CompanyAdmin)
