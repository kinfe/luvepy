from modeltranslation.translator import register, TranslationOptions
from .models import Company


@register(Company)
class CompanyTranslationOptions(TranslationOptions):
    fields = ('short_description', 'description')
