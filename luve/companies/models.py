from django.db import models

from django_countries.fields import CountryField


class Company(models.Model):
    name = models.CharField(max_length=200)
    type = models.PositiveSmallIntegerField(choices=((1, 'produttive'), (2, 'commerciali')), default=1)
    city = models.CharField(max_length=50)
    country = CountryField()
    logo = models.ImageField(upload_to='companies', blank=True)
    logo_alt = models.ImageField(upload_to='companies', blank=True, help_text='Opzionale; per sfondi scuri.')
    link = models.URLField(blank=True)
    short_description = models.TextField(blank=True)
    description = models.TextField(blank=True)
    contacts = models.TextField(blank=True)
    x = models.PositiveSmallIntegerField(blank=True, null=True)
    y = models.PositiveSmallIntegerField(blank=True, null=True)

    class Meta:
        ordering = ['type', 'name']
        verbose_name = 'società'
        verbose_name_plural = 'società'

    def __str__(self):
        return self.name
