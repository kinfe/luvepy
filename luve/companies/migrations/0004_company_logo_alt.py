# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0003_auto_20150902_0122'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='logo_alt',
            field=models.ImageField(blank=True, help_text='Opzionale; per sfondi scuri.', upload_to='companies'),
        ),
    ]
