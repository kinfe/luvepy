# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=200)),
                ('type', models.PositiveSmallIntegerField(choices=[(1, 'produttive'), (2, 'commerciali')], default=1)),
                ('city', models.CharField(max_length=50)),
                ('country', django_countries.fields.CountryField(max_length=2)),
                ('logo', models.ImageField(upload_to='companies')),
                ('short_description', models.TextField(blank=True)),
                ('short_description_it', models.TextField(null=True, blank=True)),
                ('short_description_en', models.TextField(null=True, blank=True)),
                ('description', models.TextField(blank=True)),
                ('description_it', models.TextField(null=True, blank=True)),
                ('description_en', models.TextField(null=True, blank=True)),
                ('contacts', models.TextField(blank=True)),
                ('x', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('y', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('order', models.PositiveIntegerField()),
            ],
            options={
                'verbose_name': 'società',
                'verbose_name_plural': 'società',
            },
        ),
    ]
